import {Routes} from '@angular/router';
import {UserLayoutComponent} from "./layouts/user-layout/user-layout.component";
import {HomeComponent} from "./user/home/home.component";
import {LoginComponent} from "./user/login/login.component";
import {RegisterUserComponent} from "./user/register-user/register-user.component";
import {JobsComponent} from "./user/jobs/jobs.component";
import {CompaniesComponent} from "./user/companies/companies.component";
import {MyJobComponent} from "./user/my-job/my-job.component";
import {NotFoundComponent} from "./components/not-found/not-found.component"
import {AdminLayoutComponent} from "./layouts/admin-layout/admin-layout.component";
import {DashboardAdminComponent} from "./admin/dashboard-admin/dashboard-admin.component";
import {DashboardEmployerComponent} from "./employer/dashboard-employer/dashboard-employer.component";
import {EmployerLayoutComponent} from "./layouts/employer-layout/employer-layout.component";
import { HomeEmployerComponent } from './employer/home-employer/home-employer.component';
import { JobDetailsComponent } from './user/job-details/job-details.component';
import { LoginEmployerComponent } from './employer/login-employer/login-employer.component';
import { RegisterEmployerComponent } from './employer/register-employer/register-employer.component';
import { CreateJobComponent } from './employer/create-job/create-job.component';
import { JobListEmployerComponent } from './employer/job-list-employer/job-list-employer.component';
import { employerGuard } from './auth-guard/employer.guard';
import { CompanyEmployerComponent } from './employer/company-employer/company-employer.component';
import { ProfileUserComponent } from './user/profile-user/profile-user.component';
import { userGuard } from './auth-guard/user.guard';
import { JobDetailsEmployerComponent } from './employer/job-details-employer/job-details-employer.component';
import { ApplyProfileEmployerComponent } from './employer/apply-profile-employer/apply-profile-employer.component';
import { SearchJobComponent } from './user/search-job/search-job.component';
import { CompanyDetailsComponent } from './user/company-details/company-details.component';
import { CompanyListAdminComponent } from './admin/company-list-admin/company-list-admin.component';
import { JobListAdminComponent } from './admin/job-list-admin/job-list-admin.component';
import { FieldsAdminComponent } from './admin/fields-admin/fields-admin.component';
import { UserListAdminComponent } from './admin/user-list-admin/user-list-admin.component';
import { EmployerListAdminComponent } from './admin/employer-list-admin/employer-list-admin.component';
import { JobCategoryComponent } from './user/job-category/job-category.component';
import { PostListEmployerComponent } from './employer/post-list-employer/post-list-employer.component';
import { CreatePostComponent } from './employer/create-post/create-post.component';
import { PostDetailsComponent } from './user/post-details/post-details.component';

export const routes: Routes = [
  {
    path: '', component: UserLayoutComponent, children: [
      { path: 'home', component: HomeComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterUserComponent },
      { path: 'jobs', component: JobsComponent },
      { path: 'jobs/:id', component: JobDetailsComponent },
      { path: 'search', component: SearchJobComponent },
      { path: 'field', component: JobCategoryComponent },
      { path: 'companies', component: CompaniesComponent },
      { path: 'posts/:id', component: PostDetailsComponent },
      { path: 'companies/:id', component: CompanyDetailsComponent },
      { path: 'my-job', component: MyJobComponent, canActivate: [userGuard] },
      { path: 'profile', component: ProfileUserComponent, canActivate: [userGuard] },
      { path: '', redirectTo: 'home', pathMatch: 'full' }
    ],
  },
  {
    path: 'admin', component: AdminLayoutComponent, children: [
      { path: 'dashboard', component: DashboardAdminComponent },
      { path: 'company-list', component: CompanyListAdminComponent },
      { path: 'job-list', component: JobListAdminComponent },
      { path: 'field-list', component: FieldsAdminComponent },
      { path: 'user-list', component: UserListAdminComponent },
      { path: 'employer-list', component: EmployerListAdminComponent },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
    ]
  },
  {
    path: 'employer', component: EmployerLayoutComponent, children: [
      { path: 'home', component: HomeEmployerComponent },
      { path: 'dashboard', component: DashboardEmployerComponent, canMatch: [employerGuard] },
      { path: 'login', component: LoginEmployerComponent },
      { path: 'register', component: RegisterEmployerComponent },
      { path: 'create-job', component: CreateJobComponent, canMatch: [employerGuard] },
      { path: 'job-list', component: JobListEmployerComponent, canMatch: [employerGuard] },
      { path: 'job-list/:id', component: JobDetailsEmployerComponent, canMatch: [employerGuard] },
      { path: 'apply/:id', component: ApplyProfileEmployerComponent, canMatch: [employerGuard] },
      { path: 'company', component: CompanyEmployerComponent, canMatch: [employerGuard] },
      { path: 'post-list', component: PostListEmployerComponent, canMatch: [employerGuard] },
      { path: 'create-post', component: CreatePostComponent, canMatch: [employerGuard] },
      { path: '', redirectTo: 'home', pathMatch: 'full' }
    ]
  },
  { path: '**', component: NotFoundComponent }
];
