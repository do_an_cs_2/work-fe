import { Component } from '@angular/core';
import { HeaderEmployerComponent } from '../../employer/header-employer/header-employer.component';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-employer-layout',
  standalone: true,
  imports: [HeaderEmployerComponent, RouterOutlet],
  templateUrl: './employer-layout.component.html',
  styleUrl: './employer-layout.component.scss'
})
export class EmployerLayoutComponent {

}
