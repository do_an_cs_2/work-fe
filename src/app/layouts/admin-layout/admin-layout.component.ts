import { Component } from '@angular/core';
import { NavigationEnd, Router, RouterLink, RouterOutlet } from '@angular/router';
import { JwtService } from '../../services/jwt.service';
import { AuthService } from '../../services/auth.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-admin-layout',
  standalone: true,
  imports: [RouterOutlet, RouterLink],
  templateUrl: './admin-layout.component.html',
  styleUrl: './admin-layout.component.scss'
})
export class AdminLayoutComponent {

  user: User = {};
  isLogged = false;

  constructor(
    private jwtService: JwtService,
    private authService: AuthService,
    private router: Router,
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.checkLoginStatus();
      }
    });
  }

  private checkLoginStatus() {
    if (this.authService.isLoggedIn()) {
      this.isLogged = true;
      const decoded = this.jwtService.decodeToken();
      if (decoded) {
        this.user.role = decoded.role;
        this.user.email = decoded.sub;
        console.log(this.user.role)
      }
    }
  }

  logout() {
    this.isLogged = false;
    this.authService.logout();
  }
}
