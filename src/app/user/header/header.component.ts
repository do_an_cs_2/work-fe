import { Component, OnInit } from '@angular/core';
import {
  NavigationEnd,
  NavigationExtras,
  Router,
  RouterLink,
  RouterLinkActive,
} from '@angular/router';
import { JwtService } from '../../services/jwt.service';
import { JwtPayload, jwtDecode } from 'jwt-decode';
import { AuthService } from '../../services/auth.service';
import { User } from '../../interfaces/user';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [ReactiveFormsModule, RouterLink, RouterLinkActive],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
})
export class HeaderComponent implements OnInit {
  user: User = {};
  isLogged = false;
  formSearch!: FormGroup;

  constructor(
    private jwtService: JwtService,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.checkLoginStatus();
      }
    });
  }

  ngOnInit(): void {
    this.formSearch = this.fb.group({
      name: ['', Validators.required],
    });
  }

  private checkLoginStatus() {
    if (this.authService.isLoggedIn()) {
      this.isLogged = true;
      const decoded = this.jwtService.decodeToken();
      if (decoded) {
        this.user.role = decoded.role;
        this.user.email = decoded.sub;
      }
    }
  }

  logout() {
    this.isLogged = false;
    this.authService.logout();
  }

  onSearch() {
    if (this.formSearch.valid) {
      const nameValue = this.formSearch.get('name')?.value;

      const navigationExtras: NavigationExtras = {
        queryParams: { name: nameValue },
      };

      this.router.navigate(['/search'], navigationExtras);
    }
  }
}
