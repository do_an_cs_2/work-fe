import { Component, Input, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Job } from '../../interfaces/job';

@Component({
  selector: 'app-job-item',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './job-item.component.html',
  styleUrl: './job-item.component.scss'
})
export class JobItemComponent {
  @Input() job!: Job;

  constructor() { }

}
