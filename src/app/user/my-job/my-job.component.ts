import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../../services/applicaionService.service';
import { JwtService } from '../../services/jwt.service';
import { RouterLink } from '@angular/router';
import { Application } from '../../interfaces/application';
import { JobItemComponent } from '../job-item/job-item.component';
import { FavoriteJob } from '../../interfaces/favorite-job';
import { FavoriteJobService } from '../../services/favorite-job.service';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-my-job',
  standalone: true,
  imports: [RouterLink, JobItemComponent],
  templateUrl: './my-job.component.html',
  styleUrl: './my-job.component.scss'
})
export class MyJobComponent implements OnInit {
  myJobs?: Application[];
  favoriteJobs?: FavoriteJob[];
  user?: User;

  constructor(private applicationService: ApplicationService,
    private jwtService: JwtService,
    private favoriteJobService: FavoriteJobService,
    private userService: UserService) { }

  ngOnInit(): void {
    const userEmail = this.jwtService.decodeToken()?.sub;
    if (userEmail) {
      this.applicationService.getApplicationByEmail(userEmail).subscribe(response => this.myJobs = response);
      this.userService.getUserByEmail(userEmail).subscribe(response => {
        this.user = response;
        this.favoriteJobService.getFavoriteJobByUser(response.userId).subscribe(response => this.favoriteJobs = response);
      })
    }

  }


}
