import { Component, OnInit } from '@angular/core';
import { Job } from '../../interfaces/job';
import { JobService } from '../../services/job.service';
import { response } from 'express';
import { JobItemComponent } from '../job-item/job-item.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-jobs',
  standalone: true,
  imports: [JobItemComponent],
  templateUrl: './jobs.component.html',
  styleUrl: './jobs.component.scss'
})
export class JobsComponent implements OnInit {
  jobList?: Job[];

  constructor(private jobService: JobService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.jobService.getAllJobs().subscribe(response => this.jobList = response);
  }

}
