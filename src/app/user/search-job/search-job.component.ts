import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobService } from '../../services/job.service';
import { response } from 'express';
import { Job } from '../../interfaces/job';
import { JobItemComponent } from '../job-item/job-item.component';

@Component({
  selector: 'app-search-job',
  standalone: true,
  imports: [JobItemComponent],
  templateUrl: './search-job.component.html',
  styleUrl: './search-job.component.scss'
})
export class SearchJobComponent implements OnInit {
  route: ActivatedRoute = inject(ActivatedRoute);
  jobService: JobService = inject(JobService);
  jobList?: Job[];

  constructor() { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const nameValue = params['name'];

      if (nameValue) {
        this.jobService.getJobByName(nameValue).subscribe(response => this.jobList = response);
      }
    });
  }


}
