import { Component, OnInit, inject} from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';
import { response } from 'express';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register-user',
  standalone: true,
  imports: [RouterLink, ReactiveFormsModule],
  templateUrl: './register-user.component.html',
  styleUrl: './register-user.component.scss'
})
export class RegisterUserComponent implements OnInit {
  authService: AuthService = inject(AuthService);
  router: Router = inject(Router);

  formRegister!: FormGroup;

  loading: boolean = false;
  errorMessage?: string;

  constructor(private fb: FormBuilder, private userService: UserService) { }

  ngOnInit(): void {
    this.formRegister = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    })
  }

  get name() { return this.formRegister.get('fullName') }
  get email() { return this.formRegister.get('email') }
  get password() { return this.formRegister.get('password') }

  onSubmit() {
    this.errorMessage = undefined;
    if (this.formRegister.valid) {
      this.loading = true;
      const userTemp = this.formRegister.value as User;
      this.userService.createUser(userTemp).subscribe({
        next: (response) => {
          const user: User = response;
          this.authService.login(this.email?.value, this.password?.value).subscribe({
            next: (response) => {
              this.authService.setToken(response.token);
              this.authService.setRefreshToken(response.refreshToken);
              console.log(response);
              this.router.navigate(['/home']);
            },
            error: (err) => {
              console.log(err)
              this.loading = false;
              this.errorMessage = "Invalid email or password.";
            },
            complete: () => {
              this.loading = false;
            }
          })
        },
        error: (err) => {
          console.log(err)
          this.loading = false;
          if (err.status === 409) {
            this.errorMessage = "Email already exists.";
          } else {
            this.errorMessage = "Server Error.";
          }
        },
        complete: () => {
          this.loading = false;
        }
      })
    } else {
      this.errorMessage = "Invalid data.";
    }
  }

}
