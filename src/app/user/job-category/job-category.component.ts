import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobService } from '../../services/job.service';
import { Job } from '../../interfaces/job';
import { JobItemComponent } from '../job-item/job-item.component';

@Component({
  selector: 'app-job-category',
  standalone: true,
  imports: [JobItemComponent],
  templateUrl: './job-category.component.html',
  styleUrl: './job-category.component.scss'
})
export class JobCategoryComponent implements OnInit {
  route: ActivatedRoute = inject(ActivatedRoute);
  jobService: JobService = inject(JobService);
  jobList?: Job[];

  constructor() { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const idField = params['id'];

      if (idField) {
        this.jobService.getJobByCategory(idField).subscribe(response => this.jobList = response);
      }
    });
  }
}
