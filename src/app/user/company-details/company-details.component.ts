import { Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from '../../services/company.service';
import { Company } from '../../interfaces/company';
import { response } from 'express';
import { Job } from '../../interfaces/job';
import { JobService } from '../../services/job.service';
import { JobItemComponent } from '../job-item/job-item.component';

@Component({
  selector: 'app-company-details',
  standalone: true,
  imports: [JobItemComponent],
  templateUrl: './company-details.component.html',
  styleUrl: './company-details.component.scss'
})
export class CompanyDetailsComponent {
  route: ActivatedRoute = inject(ActivatedRoute);
  companyService: CompanyService = inject(CompanyService);
  jobService: JobService = inject(JobService);

  company?: Company;
  jobList?: Job[];

  constructor() {
    const companyId = Number(this.route.snapshot.params['id']);
    this.companyService.getCompanyById(companyId).subscribe(response => this.company = response);
    this.jobService.getJobByCompany(companyId).subscribe(response => this.jobList = response);
  }
}
