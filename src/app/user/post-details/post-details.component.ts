import { Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Post } from '../../interfaces/post';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-post-details',
  standalone: true,
  imports: [DatePipe],
  templateUrl: './post-details.component.html',
  styleUrl: './post-details.component.scss'
})
export class PostDetailsComponent {
  route: ActivatedRoute = inject(ActivatedRoute);
  postService: PostService = inject(PostService);
  post?: Post;

  constructor() {
    const postId = Number(this.route.snapshot.params['id']);
    this.postService.getPostById(postId).subscribe(response => this.post = response);
  }

}
