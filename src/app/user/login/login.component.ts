import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [RouterLink, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {
  formLogin!: FormGroup;

  loading: boolean = false;
  errorMessage?: string;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.formLogin = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    })
  }

  get email() { return this.formLogin.get('email') }
  get password() { return this.formLogin.get('password') }

  onSubmit() {
    this.errorMessage = undefined;
    if (this.formLogin.valid) {
      this.loading = true;
      const userTemp = this.formLogin.value as User;
      this.authService.login(this.email?.value, this.password?.value).subscribe({
        next: (response) => {
          this.authService.setToken(response.token);
          this.authService.setRefreshToken(response.refreshToken);
          console.log(response);
          this.router.navigate(['/home']);
        },
        error: (err) => {
          console.log(err)
          this.loading = false;
          this.errorMessage = "Invalid email or password.";
        },
        complete: () => {
          this.loading = false;
        }
      })
    } else {
      this.errorMessage = "Invalid data.";
    }
  }
}
