import { Component, ElementRef, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Job } from '../../interfaces/job';
import { JobService } from '../../services/job.service';
import { response } from 'express';
import { DatePipe, NgClass } from '@angular/common';
import { AuthService } from '../../services/auth.service';
import { JwtService } from '../../services/jwt.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Application } from '../../interfaces/application';
import { ApplicationService } from '../../services/applicaionService.service';
import { FavoriteJobService } from '../../services/favorite-job.service';
import { User } from '../../interfaces/user';
import { UserService } from '../../services/user.service';
import { FavoriteJob } from '../../interfaces/favorite-job';

@Component({
  selector: 'app-job-details',
  standalone: true,
  imports: [DatePipe, NgClass, ReactiveFormsModule],
  templateUrl: './job-details.component.html',
  styleUrl: './job-details.component.scss'
})
export class JobDetailsComponent implements OnInit {
  route: ActivatedRoute = inject(ActivatedRoute);
  jobService: JobService = inject(JobService);
  authService: AuthService = inject(AuthService);
  jwtService: JwtService = inject(JwtService);
  router: Router = inject(Router);
  applicationService: ApplicationService = inject(ApplicationService);
  favoriteService: FavoriteJobService = inject(FavoriteJobService);
  userService: UserService = inject(UserService);

  job?: Job;
  user?: User;
  showModalApply: boolean = false;
  isLogged: boolean = false;
  isApplied: boolean = false;
  favoriteJob?: FavoriteJob;

  formApply!: FormGroup;
  errorMessage?: string;
  loading: boolean = false;
  cvSelected?: File;

  constructor(private fb: FormBuilder) {
    const jobId = Number(this.route.snapshot.params['id']);
    this.jobService.getJobById(jobId).subscribe(response => this.job = response);
    this.isLogged = this.authService.isLoggedIn();
    const userEmail = this.jwtService.decodeToken()?.sub;
    if (userEmail) {
      this.applicationService.getApplicationByEmail(userEmail).subscribe(response => {
        const applications: Application[] = response;
        const application = applications.find(application => application.job?.jobId === jobId);
        if (application) {
          this.isApplied = true;
        }
      });
      this.userService.getUserByEmail(userEmail).subscribe(response => {
        this.user = response;
        this.favoriteService.getFavoriteJob(response.userId, jobId).subscribe(response => this.favoriteJob = response);
      })
    }
  }

  ngOnInit(): void {
    this.formApply = this.fb.group({
      coverLetter: ['', Validators.required],
      cv: [null, Validators.required]
    })
  }

  onApply() {
    if (!this.isLogged) {
      this.router.navigate(['/login']);
      return;
    } else {
      this.showModalApply = true;
    }
  }

  get coverLetter() { return this.formApply.get('coverLetter') }

  selectCV(event: any) {
    this.cvSelected = event.target.files[0];
  }

  apply() {
    this.errorMessage = undefined;
    if (this.formApply.valid) {
      this.loading = true
      const userEmail = this.jwtService.decodeToken()?.sub;
      if (userEmail && this.job?.jobId && this.cvSelected) {
        this.applicationService.createApplication(userEmail, this.job?.jobId, this.coverLetter?.value, this.cvSelected).subscribe({
          next: (response) => {
            console.log(response);
            this.router.navigate(['/my-job']);
          },
          error: (err) => {
            console.log(err);
            this.loading = false;
          },
          complete: () => {
            this.loading = false;
          }
        });
      } else {
        this.errorMessage = 'What???';
      }
    } else {
      this.errorMessage = 'Invalid data.'
    }
  }

  heartClick() {
    if (!this.isLogged) {
      this.router.navigate(['/login']);
      return;
    }
    if (this.favoriteJob) {
      this.favoriteService.deleteFavoriteJobById(this.favoriteJob.id).subscribe(response => {
        this.favoriteJob = undefined;
        alert('You have removed from favorites.')
      });
    } else {
      if (this.user?.userId && this.job?.jobId) {
        this.favoriteService.createFavoriteJob(this.user?.userId, this.job?.jobId).subscribe(response => {
          this.favoriteJob = response;
          alert('You have added to favorites.');
        });
      }
    }

  }

}
