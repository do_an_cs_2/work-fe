import { Component, OnInit, inject } from '@angular/core';
import { User } from '../../interfaces/user';
import { UserService } from '../../services/user.service';
import { JwtService } from '../../services/jwt.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { response } from 'express';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-profile-user',
  standalone: true,
  imports: [ReactiveFormsModule, FormsModule, DatePipe],
  templateUrl: './profile-user.component.html',
  styleUrl: './profile-user.component.scss',
})
export class ProfileUserComponent implements OnInit {
  userService: UserService = inject(UserService);
  jwtService: JwtService = inject(JwtService);
  router: Router = inject(Router);
  fb: FormBuilder = inject(FormBuilder);

  user?: User;
  formUpdate!: FormGroup;
  message?: string;

  constructor() {
    const decoded = this.jwtService.decodeToken();
    if (decoded?.sub) {
      this.userService
        .getUserByEmail(decoded?.sub)
        .subscribe((response) => (this.user = response));
    }
  }

  ngOnInit(): void {
    this.formUpdate = this.fb.group({
      fullName: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      gender: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
    });
  }

  get fullName() {
    return this.formUpdate.get('fullName');
  }
  get dateOfBirth() {
    return this.formUpdate.get('dateOfBirth');
  }
  get gender() {
    return this.formUpdate.get('gender');
  }
  get phone() {
    return this.formUpdate.get('phone');
  }
  get address() {
    return this.formUpdate.get('address');
  }

  openModal() {
    this.fullName?.setValue(this.user?.fullName);
    this.dateOfBirth?.setValue(this.user?.dateOfBirth);
    this.gender?.setValue(this.user?.gender);
    this.phone?.setValue(this.user?.phone);
    this.address?.setValue(this.user?.address);
  }

  onSubmit() {
    this.message = undefined
    if (this.formUpdate.valid) {
      const data = this.formUpdate.value as User;
      if (this.user?.userId) {
        this.userService.updateUser(this.user?.userId, data).subscribe({
          next: (response) => {
            this.user = response;
            this.message = 'Updated Successfully!';
          }
        })

      }
    }
  }
}
