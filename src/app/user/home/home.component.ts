import { Component } from '@angular/core';
import { JobItemComponent } from '../job-item/job-item.component';
import { Job } from '../../interfaces/job';
import { JobService } from '../../services/job.service';
import { Company } from '../../interfaces/company';
import { CompanyService } from '../../services/company.service';
import { RouterLink } from '@angular/router';
import { ApplicationService } from '../../services/applicaionService.service';
import { Field } from '../../interfaces/field';
import { FieldService } from '../../services/field.service';
import { Post } from '../../interfaces/post';
import { PostService } from '../../services/post.service';
import { PostItemComponent } from '../post-item/post-item.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [JobItemComponent, RouterLink, PostItemComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  jobList?: Job[];
  companies?: Company[];
  fieldList?: Field[];
  postList?: Post[];

  constructor(private jobService: JobService, private companyService: CompanyService, private fieldService: FieldService, private postService: PostService) { }

  ngOnInit(): void {
    this.jobService.getAllJobs().subscribe(response => this.jobList = response);
    this.companyService.getAllCompanies().subscribe(response => this.companies = response);
    this.fieldService.getAllFields().subscribe(response => this.fieldList = response);
    this.postService.getAllPosts().subscribe(response => this.postList = response);
  }

}
