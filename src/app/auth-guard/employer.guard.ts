import { CanActivateFn, Router } from '@angular/router';
import { EmployerService } from '../services/employer.service';
import { inject } from '@angular/core';

export const employerGuard: CanActivateFn = (route, state) => {
  let employerService =  inject(EmployerService);
  let router = inject(Router);

  if (employerService.getEmployer) {
    return true;
  }
  router.navigate(['/employer/login']);
  return false;
};
