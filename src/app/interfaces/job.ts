import { Company } from "./company";
import { Field } from "./field";

export interface Job {
    jobId?: number,
    name?: string,
    salary?: string,
    location?: string,
    description?: string,
    offer?: string,
    requirement?: string,
    field?: Field,
    company?: Company
    createdAt?: Date
}
