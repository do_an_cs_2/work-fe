export interface Post {
    postId?: number,
    title?: string,
    introduction?: string,
    content?: string,
    createdAt?: Date,
    employerId?: number
}