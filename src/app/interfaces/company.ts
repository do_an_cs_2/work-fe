export interface Company {
    companyId?: number;
    name?: string;
    website?: string;
    location?: string;
    logo: File | null;
    banner: File | null;
    description?: string
}
