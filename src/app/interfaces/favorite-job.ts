import { Job } from "./job";
import { User } from "./user";

export interface FavoriteJob {
    id: number,
    user: User,
    job: Job
}