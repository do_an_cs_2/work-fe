import { Job } from "./job";
import { User } from "./user";

export interface Application {
  applicationId?: number,
  user?: User,
  job?: Job,
  coverLetter?: string,
  cv: File | null,
  status: string,
}
