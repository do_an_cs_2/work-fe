export interface User {
  userId?: number;
  fullName?: string;
  email?: string;
  password?: string;
  role?: string;
  phone?: string;
  dateOfBirth?: Date;
  gender?: string;
  address?: string;
  createdAt?: Date;
}
