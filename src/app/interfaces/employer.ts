import { Company } from './company';

export interface Employer {
  employerId?: number;
  name?: string;
  email?: string;
  password?: string;
  company?: Company;
  createdAt?: Date;
}
