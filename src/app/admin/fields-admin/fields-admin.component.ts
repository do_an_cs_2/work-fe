import { Component, OnInit, inject } from '@angular/core';
import { FieldService } from '../../services/field.service';
import { Field } from '../../interfaces/field';

@Component({
  selector: 'app-fields-admin',
  standalone: true,
  imports: [],
  templateUrl: './fields-admin.component.html',
  styleUrl: './fields-admin.component.scss'
})
export class FieldsAdminComponent implements OnInit {

  fieldService: FieldService = inject(FieldService);
  fieldList: Field[] = [];
  fieldSelected?: Field;

  ngOnInit(): void {
    this.fieldService.getAllFields().subscribe(response => this.fieldList = response);
  }

  changeFieldSelect(field: Field) {
    this.fieldSelected = field;
  }

  createField() {

  }

  deleteField() {
    if (this.fieldSelected?.fieldId) {
      this.fieldService.deleteFieldById(this.fieldSelected.fieldId).subscribe({
        next: (response) => {
          if (this.fieldList) {
            const index = this.fieldList.findIndex(
              (field) => field.fieldId === this.fieldSelected?.fieldId
            );
            if (index !== -1) {
              this.fieldList.splice(index, 1);
            }
            this.fieldList = this.fieldList.filter(
              (field) => field.fieldId !== this.fieldSelected?.fieldId
            );
          }
          console.log(response);
        },
        error: (err) => {
          console.log(err);
        },
      });
    }
  }

}
