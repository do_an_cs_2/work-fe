import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldsAdminComponent } from './fields-admin.component';

describe('FieldsAdminComponent', () => {
  let component: FieldsAdminComponent;
  let fixture: ComponentFixture<FieldsAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FieldsAdminComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FieldsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
