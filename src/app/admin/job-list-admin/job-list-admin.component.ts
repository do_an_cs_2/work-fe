import { Component, OnInit, inject } from '@angular/core';
import { JobService } from '../../services/job.service';
import { Job } from '../../interfaces/job';
import { DatePipe } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-job-list-admin',
  standalone: true,
  imports: [DatePipe, RouterLink],
  templateUrl: './job-list-admin.component.html',
  styleUrl: './job-list-admin.component.scss'
})
export class JobListAdminComponent implements OnInit {
  jobService: JobService = inject(JobService);
  jobList: Job[] = [];
  jobSelected?: Job;

  ngOnInit(): void {
    this.jobService.getAllJobs().subscribe(response => this.jobList = response);
  }

  changeJobSelect(job: Job) {
    this.jobSelected = job;
  }

  deleteJob() {
    if (this.jobSelected?.jobId) {
      this.jobService.deleteJobById(this.jobSelected.jobId).subscribe({
        next: (response) => {
          if (this.jobList) {
            const index = this.jobList.findIndex(
              (job) => job.jobId === this.jobSelected?.jobId
            );
            if (index !== -1) {
              this.jobList.splice(index, 1);
            }
            this.jobList = this.jobList.filter(
              (job) => job.jobId !== this.jobSelected?.jobId
            );
          }
          console.log(response);
        },
        error: (err) => {
          console.log(err);
        },
      });
    }
  }

}
