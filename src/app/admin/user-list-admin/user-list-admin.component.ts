import { DatePipe } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-user-list-admin',
  standalone: true,
  imports: [DatePipe, RouterLink],
  templateUrl: './user-list-admin.component.html',
  styleUrl: './user-list-admin.component.scss',
})
export class UserListAdminComponent implements OnInit {
  userService: UserService = inject(UserService);
  userList: User[] = [];
  userSelected?: User;

  ngOnInit(): void {
    this.userService
      .getAllUsers()
      .subscribe((response) => (this.userList = response));
  }

  changeUserSelect(user: User) {
    this.userSelected = user;
  }

  deleteUser() {
    if (this.userSelected?.userId) {
      this.userService.deleteUserById(this.userSelected.userId).subscribe({
        next: (response) => {
          if (this.userList) {
            const index = this.userList.findIndex(
              (user) => user.userId === this.userSelected?.userId
            );
            if (index !== -1) {
              this.userList.splice(index, 1);
            }
            this.userList = this.userList.filter(
              (user) => user.userId !== this.userSelected?.userId
            );
          }
          console.log(response);
        },
        error: (err) => {
          console.log(err);
        },
      });
    }
  }
}
