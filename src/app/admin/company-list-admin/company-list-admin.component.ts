import { Component, OnInit, inject } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { Company } from '../../interfaces/company';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-company-list-admin',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './company-list-admin.component.html',
  styleUrl: './company-list-admin.component.scss'
})
export class CompanyListAdminComponent implements OnInit {
  companyService: CompanyService = inject(CompanyService);
  companyList: Company[] = [];
  companySelected?: Company;

  ngOnInit(): void {
    this.companyService.getAllCompanies().subscribe(response => this.companyList = response);
  }

  changeCompanySelect(company: Company) {
    this.companySelected = company;
  }

  deleteCompany() {
    if (this.companySelected?.companyId) {
      this.companyService.deleteCompanyById(this.companySelected.companyId).subscribe({
        next: (response) => {
          if (this.companyList) {
            const index = this.companyList.findIndex(
              (company) => company.companyId === this.companySelected?.companyId
            );
            if (index !== -1) {
              this.companyList.splice(index, 1);
            }
            this.companyList = this.companyList.filter(
              (company) => company.companyId !== this.companySelected?.companyId
            );
          }
          console.log(response);
        },
        error: (err) => {
          console.log(err);
        },
      });
    }
  }
}
