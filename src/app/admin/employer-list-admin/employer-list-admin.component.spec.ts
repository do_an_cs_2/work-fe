import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerListAdminComponent } from './employer-list-admin.component';

describe('EmployerListAdminComponent', () => {
  let component: EmployerListAdminComponent;
  let fixture: ComponentFixture<EmployerListAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmployerListAdminComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EmployerListAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
