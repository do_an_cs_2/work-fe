import { DatePipe } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { EmployerService } from '../../services/employer.service';
import { Employer } from '../../interfaces/employer';

@Component({
  selector: 'app-employer-list-admin',
  standalone: true,
  imports: [RouterLink, DatePipe],
  templateUrl: './employer-list-admin.component.html',
  styleUrl: './employer-list-admin.component.scss'
})
export class EmployerListAdminComponent implements OnInit {
  employerService: EmployerService = inject(EmployerService);
  employerList: Employer[] = [];
  employerSelected?: Employer;

  ngOnInit(): void {
    this.employerService.getAllEmployers().subscribe(response => this.employerList = response);
  }

  changeEmployerSelect(employer: Employer) {
    this.employerSelected = employer;
  }

  deleteEmployer() {
    if (this.employerSelected?.employerId) {
      this.employerService.deleteEmployerById(this.employerSelected.employerId).subscribe({
        next: (response) => {
          if (this.employerList) {
            const index = this.employerList.findIndex(
              (employer) => employer.employerId === this.employerSelected?.employerId
            );
            if (index !== -1) {
              this.employerList.splice(index, 1);
            }
            this.employerList = this.employerList.filter(
              (employer) => employer.employerId !== this.employerSelected?.employerId
            );
          }
          console.log(response);
        },
        error: (err) => {
          console.log(err);
        },
      });
    }
  }
}
