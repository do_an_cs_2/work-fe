import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { JobService } from '../../services/job.service';
import { PostService } from '../../services/post.service';
import { EmployerService } from '../../services/employer.service';
import { ApplicationService } from '../../services/applicaionService.service';
import { Job } from '../../interfaces/job';
import { Post } from '../../interfaces/post';
import { Application } from '../../interfaces/application';

@Component({
  selector: 'app-dashboard-admin',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './dashboard-admin.component.html',
  styleUrl: './dashboard-admin.component.scss'
})
export class DashboardAdminComponent implements OnInit{

  constructor(
    private jobService: JobService,
    private postService: PostService,
    private employerService: EmployerService,
    private applicationService: ApplicationService
  ) {}

  jobList?: Job[];
  postList?: Post[];
  applicationList: Application[] = [];

  ngOnInit(): void {
      this.jobService.getAllJobs().subscribe(jobs => this.jobList = jobs);
      this.postService.getAllPosts().subscribe(posts => this.postList = posts);
      this.applicationService.getAllApplications().subscribe(applications => this.applicationList = applications);
  }
}
