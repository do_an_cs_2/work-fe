import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private urlApi = 'http://localhost:8080/api/v1/users';

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<any> {
    return this.http.get(this.urlApi);
  }

  getUserById(id: number): Observable<any> {
    return this.http.get(`${this.urlApi}/${id}`);
  }

  getUserByEmail(email: string): Observable<any> {
    return this.http.get(`${this.urlApi}/email?email=${email}`);
  }

  createUser(user: User): Observable<any> {
    return this.http.post(this.urlApi, user);
  }

  updateUser(id: number, user: User) {
    return this.http.put(`${this.urlApi}/${id}`, user);
  }

  deleteUserById(id: number): Observable<any> {
    return this.http.delete(`${this.urlApi}/${id}`);
  }
}
