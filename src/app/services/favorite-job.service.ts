import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FavoriteJobService {

  private apiUrl = 'http://localhost:8080/api/v1/favorite-job';

  constructor(private http: HttpClient) { }

  getAllFavoriteJobs(): Observable<any> {
    return this.http.get(this.apiUrl);
  }

  getFavoriteJobById(id: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/${id}`);
  }

  getFavoriteJobByUser(userId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/user/${userId}`);
  }

  createFavoriteJob(userId: number, jobId: number): Observable<any> {
    const body = { userId, jobId }
    return this.http.post(this.apiUrl, body);
  }

  deleteFavoriteJobById(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

  getFavoriteJob(userId: number, jobId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/check/${userId}/${jobId}`);
  }
}
