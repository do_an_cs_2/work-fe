import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Job } from '../interfaces/job';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  private urlApi = 'http://localhost:8080/api/v1/jobs';

  constructor(private http: HttpClient) { }

  createJob(job: Job): Observable<any> {
    return this.http.post(this.urlApi, job);
  }

  getAllJobs(): Observable<any> {
    return this.http.get(this.urlApi);
  }

  getJobByCompany(companyId: number): Observable<any> {
    return this.http.get(`${this.urlApi}/?companyId=${companyId}`);
  }

  getJobById(jobId: number): Observable<any> {
    return this.http.get(`${this.urlApi}/${jobId}`);
  }

  getJobByName(name: string): Observable<any> {
    return this.http.get(`${this.urlApi}/search?name=${name}`);
  }

  getJobByCategory(id: string): Observable<any> {
    return this.http.get(`${this.urlApi}/field?id=${id}`);
  }

  deleteJobById(id: number): Observable<any> {
    return this.http.delete(`${this.urlApi}/${id}`);
  }

}
