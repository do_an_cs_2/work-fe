import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { jwtDecode } from 'jwt-decode';
import { JwtPayload } from '../interfaces/jwt-payload';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(private authService: AuthService) { }

  decodeToken() {
    const token = this.authService.getAccessToken();
    if (token) {
      const decoded = jwtDecode<JwtPayload>(token);
      return decoded;
    }
    return null;
  }

}
