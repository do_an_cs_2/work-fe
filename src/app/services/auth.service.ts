import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private urlApi = 'http://localhost:8080/api/v1/auth';

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    const body = { email, password }
    return this.http.post(this.urlApi, body);
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  setRefreshToken(refreshToken: string) {
    localStorage.setItem('refresh-token', refreshToken);
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('refresh-token');
  }

  isLoggedIn() {
    if (typeof localStorage !== 'undefined') {
      return localStorage.getItem('token') !== null;
    }
    return false;
  }

  getAccessToken() {
    if (typeof localStorage !== 'undefined') {
      return localStorage.getItem('token');
    }
    return null;
  }

  getRefreshToken() {
    if (typeof localStorage !== 'undefined') {
      return localStorage.getItem('refresh-token');
    }
    return null;
  }

}
