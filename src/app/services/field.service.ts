import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class FieldService {
    private apiUrl = 'http://localhost:8080/api/v1/fields';

    constructor(private http: HttpClient) { }

    getAllFields(): Observable<any> {
        return this.http.get(this.apiUrl);
    }

    createField(name: string): Observable<any> {
      return this.http.post(this.apiUrl, { name: name });
    }

    deleteFieldById(id: number): Observable<any> {
      return this.http.delete(`${this.apiUrl}/${id}`);
    }
}
