import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class ApplicationService {
  private apiUrl = 'http://localhost:8080/api/v1/applications'

  constructor(private http: HttpClient) { }

  getAllApplications(): Observable<any> {
    return this.http.get(`${this.apiUrl}/`);
  }

  getApplicationByEmail(email: string): Observable<any> {
    return this.http.get(`${this.apiUrl}?email=${email}`);
  }

  getApplicationByJob(jobId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/job?id=${jobId}`);
  }

  getApplicationById(id: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/${id}`);
  }

  createApplication(userEmail: string, jobId: number, coverLetter: string, cv: File): Observable<any> {
    const body = new FormData();
    body.append('email', userEmail);
    body.append('jobId', jobId.toString());
    body.append('coverLetter', coverLetter);
    body.append('cv', cv);

    return this.http.post(this.apiUrl, body);
  }

  updateStatusApplication(id: number, status: string): Observable<any> {
    return this.http.put(`${this.apiUrl}/${id}`, { status: status});
  }

  deleteApplyById(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

}
