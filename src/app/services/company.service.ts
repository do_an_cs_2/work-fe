import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Company } from '../interfaces/company';

@Injectable({
  providedIn: 'root',
})
export class CompanyService {
  private urlApi = 'http://localhost:8080/api/v1/companies';

  constructor(private http: HttpClient) {}

  getAllCompanies(): Observable<any> {
    return this.http.get(this.urlApi);
  }

  getCompanyById(id: number): Observable<any> {
    return this.http.get(`${this.urlApi}/${id}`);
  }

  createCompany(company: Company): Observable<any> {
    if (company) {
      const formData = new FormData();
      if (company.name) formData.append('name', company.name);
      if (company.website) formData.append('website', company.website);
      if (company.location) formData.append('location', company.location);
      if (company.logo) formData.append('logo', company.logo);
      return this.http.post(this.urlApi, formData);
    }
    return of();
  }

  updateCompany(id: number, company: Company) {
    const formData = new FormData();
    if (company.name) formData.append('name', company.name);
    if (company.website) formData.append('website', company.website);
    if (company.location) formData.append('location', company.location);
    if (company.banner) formData.append('banner', company.banner);
    if (company.description) formData.append('description', company.description);
    return this.http.put(`${this.urlApi}/${id}`, formData);
  }

  deleteCompanyById(id: number): Observable<any> {
    return this.http.delete(`${this.urlApi}/${id}`);
  }
}
