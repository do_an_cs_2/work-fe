import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employer } from '../interfaces/employer';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployerService {
  private urlApi = 'http://localhost:8080/api/v1/employers';
  private employer?: Employer;

  constructor(private http: HttpClient) { }

  getAllEmployers(): Observable<any> {
    return this.http.get(this.urlApi);
  }

  createEmployer(employer: Employer): Observable<any> {
    return this.http.post(this.urlApi, employer);
  }

  login(email: string, password: string): Observable<any> {
    const body = { email, password };
    return this.http.post(`${this.urlApi}/login`, body);
  }

  setEmployer(employer: Employer) {
    this.employer = employer;
  }

  get getEmployer() {
    return this.employer;
  }

  removeEmployer() {
    this.employer = undefined;
  }

  deleteEmployerById(id: number): Observable<any> {
    return this.http.delete(`${this.urlApi}/id`);
  }

}
