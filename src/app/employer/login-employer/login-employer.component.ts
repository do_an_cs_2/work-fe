import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { EmployerService } from '../../services/employer.service';
import { response } from 'express';
import { NgClass } from '@angular/common';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-login-employer',
  standalone: true,
  imports: [ReactiveFormsModule, NgClass, RouterLink],
  templateUrl: './login-employer.component.html',
  styleUrl: './login-employer.component.scss'
})
export class LoginEmployerComponent implements OnInit {
  formLogin!: FormGroup;

  errorMessage?: string;
  loading: boolean = false;

  constructor(private fb: FormBuilder, private employerService: EmployerService, private router: Router) { }

  ngOnInit(): void {
    this.formLogin = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })
  }

  get email() { return this.formLogin.get('email') }
  get password() { return this.formLogin.get('password') }

  onSubmit() {
    this.errorMessage = '';
    if (this.formLogin.valid) {
      this.loading = true;
      this.employerService.login(this.email?.value, this.password?.value).subscribe({
        next: (response) => {
          this.employerService.setEmployer(response);
          this.router.navigate(['/employer/dashboard']);
        },
        error: (err) => {
          console.log(err);
          this.errorMessage = 'Invalid email or password';
          this.loading = false
        },
        complete: () => {
          this.loading = false
        }
      })
    } else {
      this.errorMessage = 'Invalid data.';
    }
  }

}
