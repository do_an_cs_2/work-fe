import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostListEmployerComponent } from './post-list-employer.component';

describe('PostListEmployerComponent', () => {
  let component: PostListEmployerComponent;
  let fixture: ComponentFixture<PostListEmployerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PostListEmployerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PostListEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
