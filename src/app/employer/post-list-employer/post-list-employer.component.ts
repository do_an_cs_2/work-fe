import { Component, OnInit, inject } from '@angular/core';
import { PostService } from '../../services/post.service';
import { Post } from '../../interfaces/post';
import { DatePipe } from '@angular/common';
import { RouterLink } from '@angular/router';
import { EmployerService } from '../../services/employer.service';

@Component({
  selector: 'app-post-list-employer',
  standalone: true,
  imports: [DatePipe, RouterLink],
  templateUrl: './post-list-employer.component.html',
  styleUrl: './post-list-employer.component.scss',
})
export class PostListEmployerComponent implements OnInit {
  employerService: EmployerService = inject(EmployerService);
  postService: PostService = inject(PostService);
  postList: Post[] = [];
  postSelected?: Post;

  ngOnInit(): void {
    if (this.employerService.getEmployer?.employerId) {
      this.postService
        .getPostByEmployer(this.employerService.getEmployer?.employerId)
        .subscribe((response) => (this.postList = response));
    }
  }

  changePostSelect(post: Post) {
    this.postSelected = post;
  }

  deletePost() {
    if (this.postSelected?.postId) {
      this.postService.deletePostById(this.postSelected.postId).subscribe({
        next: (response) => {
          if (this.postList) {
            const index = this.postList.findIndex(
              (post) => post.postId === this.postSelected?.postId
            );
            if (index !== -1) {
              this.postList.splice(index, 1);
            }
            this.postList = this.postList.filter(
              (post) => post.postId !== this.postSelected?.postId
            );
          }
          console.log(response);
        },
        error: (err) => {
          console.log(err);
        },
      });
    }
  }
}
