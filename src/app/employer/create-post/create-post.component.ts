import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { PostService } from '../../services/post.service';
import { Post } from '../../interfaces/post';
import { EmployerService } from '../../services/employer.service';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-create-post',
  standalone: true,
  imports: [ReactiveFormsModule, FormsModule, NgClass],
  templateUrl: './create-post.component.html',
  styleUrl: './create-post.component.scss',
})
export class CreatePostComponent implements OnInit {
  formPost!: FormGroup;

  message: string = '';
  loading: boolean = false;
  submitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private postService: PostService,
    private employerService: EmployerService
  ) {}

  ngOnInit(): void {
    this.formPost = this.fb.group({
      title: ['', Validators.required],
      introduction: ['', Validators.required],
      content: ['', Validators.required],
    });
  }

  get title() { return this.formPost.get('title') }
  get introduction() { return this.formPost.get('introduction') }
  get content() { return this.formPost.get('content') }

  onSubmit(): void {
    this.message = '';
    this.submitted = true;
    if (this.formPost.valid) {
      this.loading = true;
      let data = this.formPost.value as Post;
      data.employerId = this.employerService.getEmployer?.employerId;
      this.postService.createPost(data).subscribe({
        next: (response) => {
          console.log(response);
          this.message = 'Created successfully!';
        },
        error: (err) => {
          console.log(err);
          this.loading = false;
        },
        complete: () => {
          this.loading = true;
          this.submitted = false;
          this.formPost.reset();
        }
      });
    } else {
      this.message = 'Invalid data.';
    }
  }
}
