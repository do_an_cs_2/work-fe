import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Company } from '../../interfaces/company';
import { CompanyService } from '../../services/company.service';
import { EmployerService } from '../../services/employer.service';
import { response } from 'express';
import { Employer } from '../../interfaces/employer';
import { switchMap } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-employer',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './register-employer.component.html',
  styleUrl: './register-employer.component.scss'
})
export class RegisterEmployerComponent implements OnInit {
  formEmployer!: FormGroup;
  formCompany!: FormGroup;

  message: string = '';
  loading: boolean = false;
  logoSelected?: File;

  constructor(private fb: FormBuilder, private companyService: CompanyService, private employerService: EmployerService, private router: Router) { }

  ngOnInit(): void {
    this.formEmployer = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(10)]]
    });

    this.formCompany = this.fb.group({
      name: ['', Validators.required],
      website: ['', Validators.required],
      location: ['', Validators.required],
      logo: [null, Validators.required]
    })
  }

  selectLogo(event: any) {
    this.logoSelected = event.target.files[0];
  }

  get email() { return this.formEmployer.get('email') }
  get password() { return this.formEmployer.get('password') }

  onSubmit() {
    this.message = '';
    if (this.formEmployer.valid && this.formCompany.valid) {
      this.loading = true;
      let company = this.formCompany.value as Company;
      if (this.logoSelected) {
        company.logo = this.logoSelected;
      }

      this.companyService.createCompany(company).pipe(
        switchMap((responseCompany: Company) => {
          let employer = this.formEmployer.value as Employer;
          console.log(responseCompany);
          employer.company = responseCompany;
          console.log(employer);
          return this.employerService.createEmployer(employer);
        })
      ).subscribe({
        next: (response) => {
          console.log('Employer created:', response);
          this.employerService.login(this.email?.value, this.password?.value).subscribe({
            next: (response) => {
              this.employerService.setEmployer(response);
              this.router.navigate(['/employer/dashboard']);
            },
            error: (err) => {
              console.log(err);
              this.loading = false
            },
            complete: () => {
              this.loading = false
            }
          })
        },
        error: (err) => {
          console.error('Error creating employer:', err);
          this.message = 'Error creating employer';
          this.loading = false;
        },
        complete: () => {
          this.loading = false;
        }
      });
    } else {
      this.message = 'Invalid data.'
    }
  }

}
