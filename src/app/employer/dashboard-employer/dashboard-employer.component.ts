import { Component, OnInit, Inject } from '@angular/core';
import { Job } from '../../interfaces/job';
import { Post } from '../../interfaces/post';
import { JobService } from '../../services/job.service';
import { PostService } from '../../services/post.service';
import { EmployerService } from '../../services/employer.service';
import { Application } from '../../interfaces/application';  // Import Application interface
import { ApplicationService } from '../../services/applicaionService.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-dashboard-employer',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './dashboard-employer.component.html',
  styleUrl: './dashboard-employer.component.scss',
})
export class DashboardEmployerComponent implements OnInit {
  constructor(
    private jobService: JobService,
    private postService: PostService,
    private employerService: EmployerService,
    private applicationService: ApplicationService
  ) {}

  jobList?: Job[];
  postList?: Post[];
  applicationList: Application[] = [];  // Initialize as an empty array

  ngOnInit(): void {
    const companyId = this.employerService.getEmployer?.company?.companyId;
    const employerId = this.employerService.getEmployer?.employerId;

    if (companyId) {
      this.jobService.getJobByCompany(companyId).subscribe((jobs) => {
        this.jobList = jobs;

        jobs.forEach((job: Job) => {
          if (job.jobId) {
            this.applicationService.getApplicationByJob(job.jobId).subscribe((applications: Application[]) => {
              this.applicationList = this.applicationList.concat(applications);
              console.log(this.applicationList);
            });
          }
        });
      });
    }

    if (employerId) {
      this.postService.getPostByEmployer(employerId).subscribe((posts) => {
        this.postList = posts;
      });
    }
  }
}
