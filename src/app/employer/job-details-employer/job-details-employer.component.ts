import { Component, inject } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Job } from '../../interfaces/job';
import { JobService } from '../../services/job.service';
import { ApplicationService } from '../../services/applicaionService.service';
import { Application } from '../../interfaces/application';

@Component({
  selector: 'app-job-details-employer',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './job-details-employer.component.html',
  styleUrl: './job-details-employer.component.scss'
})
export class JobDetailsEmployerComponent {
  route: ActivatedRoute = inject(ActivatedRoute);
  jobService: JobService = inject(JobService);
  applicationService: ApplicationService = inject(ApplicationService);

  job?: Job;
  applicationList?: Application[];

  constructor() {
    const jobId = Number(this.route.snapshot.params['id']);
    this.jobService.getJobById(jobId).subscribe(response => this.job = response);
    this.applicationService.getApplicationByJob(jobId).subscribe(response => this.applicationList = response);
  }

}
