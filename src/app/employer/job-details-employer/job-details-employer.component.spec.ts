import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDetailsEmployerComponent } from './job-details-employer.component';

describe('JobDetailsEmployerComponent', () => {
  let component: JobDetailsEmployerComponent;
  let fixture: ComponentFixture<JobDetailsEmployerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JobDetailsEmployerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(JobDetailsEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
