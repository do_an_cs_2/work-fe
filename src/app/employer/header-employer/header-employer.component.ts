import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterLink } from '@angular/router';
import { Employer } from '../../interfaces/employer';
import { EmployerService } from '../../services/employer.service';

@Component({
  selector: 'app-header-employer',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './header-employer.component.html',
  styleUrl: './header-employer.component.scss'
})
export class HeaderEmployerComponent {
  employer?: Employer;

  constructor(private employerService: EmployerService, private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.checkLoginStatus();
      }
    });
  }

  logout() {
    this.employerService.removeEmployer();
    this.employer = undefined;
    this.router.navigate(['/employer/login']);
  }

  private checkLoginStatus() {
    if (this.employerService.getEmployer != null) {
      this.employer = this.employerService.getEmployer;
    }
  }
}
