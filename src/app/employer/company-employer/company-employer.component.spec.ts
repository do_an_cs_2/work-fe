import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyEmployerComponent } from './company-employer.component';

describe('CompanyEmployerComponent', () => {
  let component: CompanyEmployerComponent;
  let fixture: ComponentFixture<CompanyEmployerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CompanyEmployerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CompanyEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
