import { Component, OnInit, inject } from '@angular/core';
import { Employer } from '../../interfaces/employer';
import { EmployerService } from '../../services/employer.service';
import { Company } from '../../interfaces/company';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { CompanyService } from '../../services/company.service';

@Component({
  selector: 'app-company-employer',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './company-employer.component.html',
  styleUrl: './company-employer.component.scss'
})
export class CompanyEmployerComponent implements OnInit {
  companyService: CompanyService = inject(CompanyService);
  fb: FormBuilder = inject(FormBuilder);

  company?: Company;
  formCompany!: FormGroup;
  bannerSelected?: File;
  errorMessage?: string;

  constructor(private employerService: EmployerService) { }

  ngOnInit(): void {
    this.company = this.employerService.getEmployer?.company;
    this.formCompany = this.fb.group({
      name: [this.company?.name, Validators.required],
      location: [this.company?.location, Validators.required],
      description: [this.company?.description],
      website: [this.company?.website, Validators.required],
      banner: [this.company?.banner]
    })
  }

  onSelectBanner(event: any) {
    this.bannerSelected = event.target.files[0];
  }

  onUpdate() {
    this.errorMessage = undefined;
    if (this.formCompany.valid) {
      this.company = this.formCompany.value as Company;
      if (this.bannerSelected) {
        this.company.banner = this.bannerSelected;
      }
      if (this.company.companyId) {
        this.companyService.updateCompany(this.company.companyId, this.company).subscribe(response => console.log(response));
      }
    } else {
      this.errorMessage = 'Invalid data.';
    }
  }

}
