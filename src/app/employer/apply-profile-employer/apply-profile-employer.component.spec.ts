import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyProfileEmployerComponent } from './apply-profile-employer.component';

describe('ApplyProfileEmployerComponent', () => {
  let component: ApplyProfileEmployerComponent;
  let fixture: ComponentFixture<ApplyProfileEmployerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ApplyProfileEmployerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ApplyProfileEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
