import { Component, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobService } from '../../services/job.service';
import { ApplicationService } from '../../services/applicaionService.service';
import { Application } from '../../interfaces/application';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-apply-profile-employer',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './apply-profile-employer.component.html',
  styleUrl: './apply-profile-employer.component.scss'
})
export class ApplyProfileEmployerComponent {
  route: ActivatedRoute = inject(ActivatedRoute);
  jobService: JobService = inject(JobService);
  router: Router = inject(Router);
  applicationService: ApplicationService = inject(ApplicationService);

  application?: Application;
  statusSelected: string = '';
  message?: string;

  constructor() {
    const applyId = Number(this.route.snapshot.params['id']);
    this.applicationService.getApplicationById(applyId).subscribe(response => this.application = response);
  }

  changeStatus() {
    this.message = undefined;
    const status = this.statusSelected;
    if (status && this.application?.applicationId) {
      this.applicationService.updateStatusApplication(this.application?.applicationId, status).subscribe({
        next: (response) => {
          this.application = response;
          this.message = 'The status has been changed successfully!';
        },
        error: (err) => {
          console.log(err)
          this.message = 'The status has been change failed!';
        }
      })
    }
  }

  deleteProfile() {
    if (this.application?.applicationId) {
      this.applicationService.deleteApplyById(this.application?.applicationId).subscribe(response => console.log(response));
      this.router.navigate(['/employer/job-list', this.application.job?.jobId]);
    }
  }

}
