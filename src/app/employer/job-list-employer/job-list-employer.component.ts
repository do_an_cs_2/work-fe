import { Job } from '../../interfaces/job';
import { JobService } from '../../services/job.service';
import { EmployerService } from '../../services/employer.service';
import { DatePipe } from '@angular/common';
import { ApplicationService } from '../../services/applicaionService.service';
import { RouterLink } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Application } from '../../interfaces/application';

@Component({
  selector: 'app-job-list-employer',
  standalone: true,
  imports: [DatePipe, RouterLink],
  templateUrl: './job-list-employer.component.html',
  styleUrl: './job-list-employer.component.scss',
})
export class JobListEmployerComponent implements OnInit {
  jobList?: Job[];
  appLication?: Application[];

  jobSelected?: Job;

  constructor(
    private jobService: JobService,
    private employerService: EmployerService,
    private applicationService: ApplicationService
  ) {}

  ngOnInit(): void {
    const companyId = this.employerService.getEmployer?.company?.companyId;
    if (companyId) {
      this.jobService
        .getJobByCompany(companyId)
        .subscribe((response) => (this.jobList = response));
    }
    this.applicationService
      .getAllApplications()
      .subscribe((response) => console.log(response));
  }

  changeJobSelect(job: Job) {
    this.jobSelected = job;
  }

  deleteJob() {
    if (this.jobSelected?.jobId) {
      this.jobService.deleteJobById(this.jobSelected.jobId).subscribe({
        next: (response) => {
          if (this.jobList) {
            const index = this.jobList.findIndex(
              (job) => job.jobId === this.jobSelected?.jobId
            );
            if (index !== -1) {
              this.jobList.splice(index, 1);
            }
            this.jobList = this.jobList.filter(
              (job) => job.jobId !== this.jobSelected?.jobId
            );
          }
          console.log(response);
        },
        error: (err) => {
          console.log(err);
        },
      });
    }
  }
}
