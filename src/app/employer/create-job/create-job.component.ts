import { Component, OnInit } from '@angular/core';
import { Field } from '../../interfaces/field';
import { FieldService } from '../../services/field.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Job } from '../../interfaces/job';
import { EmployerService } from '../../services/employer.service';
import { JobService } from '../../services/job.service';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-create-job',
  standalone: true,
  imports: [ReactiveFormsModule, NgClass],
  templateUrl: './create-job.component.html',
  styleUrl: './create-job.component.scss'
})
export class CreateJobComponent implements OnInit {
  formJob!: FormGroup;
  fieldList: Field[] = [];

  message?: string;
  isError: boolean = false;
  loading: boolean = false;

  constructor(private fieldService: FieldService, private fb: FormBuilder, private employerService: EmployerService, private jobService: JobService) {
    this.formJob = this.fb.group({
      name: ['', Validators.required],
      salary: ['', Validators.required],
      location: ['', Validators.required],
      description: ['', Validators.required],
      offer: ['', Validators.required],
      requirement: ['', Validators.required],
      field: ['', Validators.required]
    })
  }

  get name() { return this.formJob.get('name') }
  get salary() { return this.formJob.get('salary') }
  get location() { return this.formJob.get('location') }
  get description() { return this.formJob.get('description') }
  get requirement() { return this.formJob.get('requirement') }
  get offer() { return this.formJob.get('offer') }
  get field() { return this.formJob.get('field') }

  ngOnInit(): void {
    this.fieldService.getAllFields().subscribe(response => this.fieldList = response);
  }

  onTextareaInput(event: Event): void {
    const textareaValue: string = (event.target as HTMLTextAreaElement).value;

    if (textareaValue.includes('\n')) {
      console.log('Có kí tự xuống dòng!');
      console.log(this.description?.value);
    }
  }

  onSubmit(): void {
    this.message = undefined;
    this.isError = false;
    if (this.formJob.valid) {
      this.loading = true;
      let jobTemp = this.formJob.value as Job;
      if (this.employerService.getEmployer){
        jobTemp.company = this.employerService.getEmployer.company;
      }
      jobTemp.field = this.fieldList[this.field?.value];
      this.jobService.createJob(jobTemp).subscribe({
        next: (response) => {
          console.log(response);
          this.message = 'Job created successfully!';
          this.loading = false;
          this.formJob.reset();
        },
        error: (err) => {
          this.loading = false;
        }
      })
    } else {
      this.isError = true;
      this.message = 'Invalid data.'
    }
  }

}
